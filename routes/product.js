const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a product
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

//Routes Archiving a product

router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})



module.exports = router;