const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	let data = {
		user: req.body,
		isAdmin: auth.verify(req.headers.authorization).isAdmin
	}
userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send (resultFromController));
})


//Route UPDATE ADMIN to USER
router.put("/:userId/setAsUser", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.addUserUser(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving user details
/*router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});*/






module.exports = router;