const express = require ("express")
const mongoose = require("mongoose")
const cors = require ("cors")
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();


// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:UVIk6OYvOr5ga8TN@cluster0.d3kcn.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


// Prompts a message for successful database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Allows all resources to access the backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/api/users", userRoutes);
app.use("/api/product", productRoutes);




// App listening to port 5000
app.listen(process.env.PORT || 5000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 5000 }`)
});
