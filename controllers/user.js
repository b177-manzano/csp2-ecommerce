// imports the User model
const User = require("../models/User");
//imports Course model
//const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// Controller USER REGISTRATION ===== Controller function for user registration  
module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		email : reqBody.email,
		isAdmin: reqBody.isAdmin,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

// Controller USER AUTHENTICATION (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			return result;
			if(isPasswordCorrect){
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}
			// Passwords do not match
			else{
				return false;
			}
		}
	})
}

module.exports.setAsAdmin = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updatedUser = {
		isAdmin: true
	}
	//return "Hello";
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
	})
}


// Controller SET USER AS AN ADMIN (Admin Only)

/*module.exports.addUserAdmin = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updatedUser = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
	})
}*/

/*module.exports.addUserAdmin = (reqParams, data) =>{

	console.log(data.isAdmin)

	if(data.isAdmin){

		let adminStatus ={isAdmin: true}

		return User.findByIdAndUpdate(reqParams.userId, adminStatus)
		
		.then((user,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	}
	else{
		return Promise.resolve(false)

	}

}
*/

// Controller for set user as admin



// Controller SET ADMIN AS AN USER (Admin Only)

/*module.exports.addUserUser = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updatedAdmin = {
		isAdmin: false
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
	})
}
*/







/*module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};
*/


/*// Controller for enrolling the user to a specific course 
module.exports.enroll = async (data) => {
	//Add the course ID in the enrollements array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId});
		//Save the updated user information in the database
		return user.save().then((user,error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	//Add the user Id in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//Adds the userId in the course's enrollees array
		course.enrollees.push({userId: data.userId});
		//Saves the updated course information in the database
		return course.save().then((course,error)=>{
			if (error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	// Condition that will if the user and course document have been updated
	//User enrollment is successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	//User enrollment failure
	else {
		return false;
	}

}
*/