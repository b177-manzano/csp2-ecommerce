const Product = require("../models/Product");

//Controller for Creating a product

module.exports.addProduct = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newProduct" and instantiates a new "Product object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price

		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation failed
			if (error) {

				return false;

			// Product creation successful
			} else {

				return true;

			};

		});
	// User is not an admin
	} else {
		return false;
	};
};

//Controller Retrieving all products
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

//Controller Retrieve a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}
// Controller function for updating a product
module.exports.updateProduct = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
	})
}

//Controller Archiving a product

module.exports.archiveProduct = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
	})
}