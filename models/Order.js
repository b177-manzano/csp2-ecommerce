const orderSchema = new mongoose.Schema({
  
  orderId: {
    type: String,
    required: true
  },  
  productsOrdered: [
    {
          productId: {
            type: String,
            required: true,
          },
          quantity: {
            type: Number,
            default: 1,
          },
          price: {
            type: Number,
            default: 0,
          }
      },   
  ],
  totalQuantity: {
      type: Number,
      default: 0,
      required: true,
  },
  totalAmount: {
      type: Number,
      default: 0,
      required: true,
  },  
  purchasedOn: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model("Order", orderSchema);