const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},

	isAdmin :{
		type : Boolean,
		default: false
	}

	// The "Order" property/field will be an array of objects containing the total amount of order, the date and time that the user order the product.
	/*order : [
		{
			totalAmount : {
				type : number,
				required : [true, "Total amount is required"]
			},
			purchaseOn : {
				type : Date,
				default : new Date()
				//timestamps : true (will try)
			},
		}
	]*/
})

module.exports = mongoose.model("User", userSchema);
